var game = new Phaser.Game(800, 640, Phaser.AUTO, 'phaser-example', { preload: preload, create: create, update: update, render: render });


function preload () {

    game.load.atlas('atlas', '/tanks/tanks.png', '/tanks/tanks.json?v=2');
    game.load.spritesheet('kaboom', '/tanks/explosion.png', 64, 64, 23);

    game.load.image('tiles', '/tanks/tiles.png');

    game.load.atlas('controls', '/tanks/controls.png', '/tanks/controls.json');

    game.load.image('leaderboard', '/tanks/leaderboard.png');
    
    // fullscreen setup
    game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
    game.scale.fullScreenScaleMode = Phaser.ScaleManager.EXACT_FIT;

    if (game.device.iOS) {
        //iOS does not support fullscreen
        game.scale.scaleMode = Phaser.ScaleManager.EXACT_FIT;
    }
}

var layer;
var map;

var myId;
var myUnit;

var explosionsCache;
var bulletsCache;
var gradesCache;
var bulletsAlive = [];
var gradesAlive = [];

var clients = {};

var spaceKey;
var cursors;
var buttonFire, buttonLeft, buttonRight, buttonDown, buttonUp, buttonFs;
var pressedLeft = false, 
pressedRight = false, 
pressedUp = false, 
pressedDown = false;
var keyboard;

var moveStartedAt = 0;
var lastMoveTime = 0;
var realMoveStoppedAt = 0;
var tankMoveSpeed = 0.1;

var leaderboardTexts = [];

function create () {
    if (!game.device.desktop){ 
        addMobileButtons(this);
    }
    
    
    game.world.setBounds(-1000, -1000, 2000, 2000);

    cursors = game.input.keyboard.createCursorKeys();
    spaceKey = game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
    //game.input.keyboard.addKeyCapture([ Phaser.Keyboard.SPACEBAR ]);
    spaceKey.onDown.add(fire, this);

    onIncomingMessage = onMessage;
}

function createLeaderBoard() {
    var bg = game.add.sprite(game.camera.width - 145, 5, 'leaderboard');
    bg.fixedToCamera = true;
    var style = {
        font: "12px Arial",
        fill: "#fff"
    };


    var x = game.camera.width - 125;
    var y = 25;
    var yo = 20;

    for (i=0; i < 8; i++) {
        var text = game.add.text(x, y + i * yo, "", style);
        text.alpha = 0.8;
        text.fixedToCamera = true;
        leaderboardTexts.push(text);
    }
}

function addMobileButtons(ctx) {
    var posLeftX = 100;
    var posLeftY = game.height - 100;
    var alpha = 0.8;

    buttonFire = game.add.button(game.width - posLeftX, posLeftY, 'controls', null, ctx, 'fire2', 'fire1', 'fire2', 'fire1');
    buttonFire.fixedToCamera = true;
    buttonFire.events.onInputDown.add(fire, this);
    buttonFire.anchor.setTo(0.5, 0.5);
    buttonFire.alpha = alpha;

    function resetButtons() {
        pressedLeft = false;
        pressedUp = false;
        pressedRight = false;
        pressedDown = false;
    }

    buttonLeft = game.add.button(posLeftX, posLeftY, 'controls', null, ctx, 'left2', 'left1', 'left2', 'left1');
    buttonLeft.fixedToCamera = true;
    buttonLeft.events.onInputDown.add(function(){resetButtons(); pressedLeft=true;});
    buttonLeft.events.onInputUp.add(function(){resetButtons(); pressedLeft=false;});
    buttonLeft.events.onInputOver.add(function(){resetButtons(); pressedLeft=true;});
    buttonLeft.events.onInputOut.add(function(){resetButtons(); pressedLeft=false;});
    buttonLeft.anchor.setTo(1, 0.5);
    buttonLeft.scale.setTo(1.2, 1.2);
    buttonLeft.alpha = alpha;

    buttonRight = game.add.button(posLeftX, posLeftY, 'controls', null, ctx, 'right2', 'right1', 'right2', 'right1');
    buttonRight.fixedToCamera = true;
    buttonRight.events.onInputDown.add(function(){resetButtons(); pressedRight=true;});
    buttonRight.events.onInputUp.add(function(){resetButtons(); pressedRight=false;});
    buttonRight.events.onInputOver.add(function(){resetButtons(); pressedRight=true;});
    buttonRight.events.onInputOut.add(function(){resetButtons(); pressedRight=false;});
    buttonRight.anchor.setTo(0, 0.5);
    buttonRight.scale.setTo(1.2, 1.2);
    buttonRight.alpha = alpha;

    buttonDown = game.add.button(posLeftX, posLeftY, 'controls', null, ctx, 'down2', 'down1', 'down2', 'down1');
    buttonDown.fixedToCamera = true;
    buttonDown.events.onInputDown.add(function(){resetButtons(); pressedDown=true;});
    buttonDown.events.onInputUp.add(function(){resetButtons(); pressedDown=false;});
    buttonDown.events.onInputOver.add(function(){resetButtons(); pressedDown=true;});
    buttonDown.events.onInputOut.add(function(){resetButtons(); pressedDown=false;});
    buttonDown.anchor.setTo(0.5, 0);
    buttonDown.scale.setTo(1.2, 1.2);
    buttonDown.alpha = alpha;

    buttonUp = game.add.button(posLeftX, posLeftY, 'controls', null, ctx, 'up2', 'up1', 'up2', 'up1');
    buttonUp.fixedToCamera = true;
    buttonUp.events.onInputDown.add(function(){resetButtons(); pressedUp=true;});
    buttonUp.events.onInputUp.add(function(){resetButtons(); pressedUp=false;});
    buttonUp.events.onInputOver.add(function(){resetButtons(); pressedUp=true;});
    buttonUp.events.onInputOut.add(function(){resetButtons(); pressedUp=false;});
    buttonUp.anchor.setTo(0.5, 1);
    buttonUp.scale.setTo(1.2, 1.2);
    buttonUp.alpha = alpha;


    buttonFs = game.add.button(posLeftX, 60, 'controls', null, ctx, 'fullscreen2', 'fullscreen1', 'fullscreen2', 'fullscreen1');
    buttonFs.fixedToCamera = true;
    buttonFs.events.onInputDown.add(gofull, this);
    buttonFs.anchor.setTo(0.5, 0.5);
    buttonFs.alpha = 0.5;
}


function onMessage (data, evt) {
    if (data.joinId) {
        onJoinedMessage(data);
    }

    if (data.tanks) {
        onTanksMessage(data.tanks);
    }

    if (data.removeTank) {
        onRemoveTankMessage(data.removeTank);
    }

    if (data.removeBullet) {
        onRemoveBulletMessage(data.removeBullet);
    }

    if (data.bullets) {
        updateBullets(data.bullets);
    }

    if (data.updateTile) {
        updateTile(data.updateTile);
    }

    if (data.destroyTank) {
        destroyTank(data.destroyTank);
    }

    if (data.lvls) {
        onLvlsMessage(data.lvls);
    }

    if (data.grade) {
        onGradeMessage(data.grade);
    }

    if (data.removeGrade) {
        onRemoveGradeMessage(data.removeGrade);
    }
}

function onTanksMessage(data) {
    for (var clientId in data) {
        if (data.hasOwnProperty(clientId)) {
            var tankPos = data[clientId];
            if (clients[clientId]) {
                updateClient(clientId, tankPos);
            } else {
                addNewClient(clientId, tankPos);
            }
        }
    }
    updateLeaderBoard();
}

function onRemoveTankMessage(clientId) {
    var unit = clients[clientId];
    if (unit) {
        unit.destroy();
        delete clients[clientId];
        //console.log("Removed tank: ", clientId);
    }
}

function onRemoveBulletMessage(bulletId) {
    for (var b = 0; b < bulletsAlive.length; b++) {
        var bullet = bulletsAlive[b];
        if (bullet.id == bulletId) {
            bullet.kill();
            bulletsAlive.splice(b, 1);
            //console.log("Removed bullet: ", bulletId);
        }
    }
}

function onGradeMessage(data) {
    spawnGrade(data.rx, data.ry);
}

function onRemoveGradeMessage(data) {
    for (var i = 0; i < gradesAlive.length; i++) {
        var g = gradesAlive[i];
        if (g.x == data.rx && g.y == data.ry) {
            g.kill();
        }
    }
}

function spawnGrade(x, y) {
    var newGrade = gradesCache.getFirstExists(false);
    if (newGrade) {
        newGrade.reset(x, y);
        gradesAlive.push(newGrade);
    }
}

function onLvlsMessage(data) {
    for (var i=0; i<data.length; i++) {
        var info = data[i];
        var unit = clients[info.id];
        if (unit && info.lvl > 0) {
            unit.updateLvl(info.lvl);
        }
    }
    updateLeaderBoard();
}

function onJoinedMessage(data) {
    myId = data.joinId;
    //console.log("My id", myId);
    
    game.cache.addTilemap('dynamicMap', null, data.map, Phaser.Tilemap.TILED_JSON);
    map = game.add.tilemap('dynamicMap', data.map.tilewidth, data.map.tileheight);
    map.addTilesetImage('tiles', 'tiles');
    layer = map.createLayer('main');
    layer.resizeWorld();
    var overLayer = map.createLayer('over');

    myUnit = new MyTankUnit(game, data);
    game.camera.follow(myUnit.tank);
    game.camera.deadzone = new Phaser.Rectangle(350, 300, 100, 80);

    onTanksMessage(data.positions.tanks);

    //  Our bullet group
    bulletsCache = game.add.group();
    bulletsCache.createMultiple(300, 'atlas', 'bullet.png', false);
    bulletsCache.setAll('anchor.x', 0.5);
    bulletsCache.setAll('anchor.y', 0.5);

    overLayer.bringToTop();

    gradesCache = game.add.group();
    gradesCache.createMultiple(300, 'atlas', 'star.png', false);

    if (data.map.grades) {
        for (var i=0; i<data.map.grades.length; i++) {
            spawnGrade(data.map.grades[i].rx, data.map.grades[i].ry);
        }
    }

    //  Explosion pool
    explosionsCache = game.add.group();

    for (var i = 0; i < 10; i++)
    {
        var explosionAnimation = explosionsCache.create(0, 0, 'kaboom', [0], false);
        explosionAnimation.anchor.setTo(0.5, 0.5);
        explosionAnimation.animations.add('kaboom');
    }

    createLeaderBoard();
    updateLeaderBoard();

    if (buttonFire) {
        buttonFire.bringToTop();
        buttonLeft.bringToTop();
        buttonUp.bringToTop();
        buttonRight.bringToTop();
        buttonDown.bringToTop();
        buttonFs.bringToTop();
    }
    
}

function destroyTank(data) {
    //console.log("destroyTank", data);
    var id = data.id;

    var otherTank = clients[id];
    var explosionAnimation = explosionsCache.getFirstExists(false);
    explosionAnimation.reset(otherTank.x, otherTank.y);
    explosionAnimation.play('kaboom', 30, false, true);

    
    if (id == myId) {
        myUnit.destroy();
        var text = game.add.text((game.camera.x + game.camera.width) / 2, (game.camera.y + game.camera.height) / 2, "GAME OVER");
        text.anchor.x = 0.5;
        text.anchor.y = 0.5;

        window.setTimeout(function(){
            location.reload();
        }, 3000);
    }
}

function updateTile(data) {
    map.putTile(data.gid, data.x, data.y)
}

function addNewClient(clientId, data) {
    if (!myId) {
        return;
    }
    var unit = new TankUnit(game, data);
    clients[clientId] = unit;
    //console.log('added client', clientId, data);
}

function updateClient(clientId, data) {
    var unit = clients[clientId];
    unit.setNewData(data);

    if (clientId == myId) {
        unit.hide();
        myUnit.setNewData(data);
    }
}

function updateBullets(bulletsData) {
    var i, b;
    var created = 0;
    var updated = 0;
    for (i = 0; i < bulletsData.length; i++) {
        var bData = bulletsData[i];
        var foundAlive = false;
        //update
        for (b = 0; b < bulletsAlive.length; b++) {
            if (bulletsAlive[b].id == bData.id) {
                bulletsAlive[b].reset(bData.x, bData.y);
                foundAlive = true;
                updated++;
            }
        }
        //create
        if (!foundAlive) {
            var newBullet = bulletsCache.getFirstExists(false);
            if (newBullet) {
                newBullet.id = bData.id;
                newBullet.reset(bData.x, bData.y);
                newBullet.angle = getAngleFromDirection(bData.dr);
                bulletsAlive.push(newBullet);
                created++;
            }
        }
    }    
}

function getAngleFromDirection(direction) {
    var angle = 0;
    if (direction == Phaser.LEFT) {
        angle = 180;
    } else if (direction == Phaser.RIGHT) {
        angle = 0;
    } else if (direction == Phaser.UP) {
        angle = -90;
    } else if (direction == Phaser.DOWN) {
        angle = 90;
    }
    return angle;
}


function update () {

    if (!myUnit) {
        moveStartedAt = 0;
        return;
    }

    var now = (new Date).getTime();

    var move = false;
    if (cursors.left.isDown || pressedLeft) {
        myUnit.direction = Phaser.LEFT;
        move = true;
    } else if (cursors.right.isDown || pressedRight) {
        myUnit.direction = Phaser.RIGHT;
        move = true;
    } else if (cursors.up.isDown || pressedUp) {
        myUnit.direction = Phaser.UP;
        move = true;
    } else if (cursors.down.isDown || pressedDown) {
        myUnit.direction = Phaser.DOWN;
        move = true;
    }

    var isHorChange = (myUnit.lastDirection == Phaser.LEFT || myUnit.lastDirection == Phaser.RIGHT) && (myUnit.direction == Phaser.UP || myUnit == Phaser.DOWN);
    var isVertChange = (myUnit.lastDirection == Phaser.UP || myUnit.lastDirection == Phaser.DOWN) && (myUnit.direction == Phaser.LEFT || myUnit == Phaser.RIGHT);
    
    if (isHorChange || isVertChange) {
        myUnit.x = myUnit.rawX = Math.round(myUnit.rawX / 16) * 16;
        myUnit.y = myUnit.rawY = Math.round(myUnit.rawY / 16) * 16;
        myUnit.update();
        moveStartedAt = 0;
    }

    myUnit.lastDirection = myUnit.direction;

    if (move) {
        if (moveStartedAt > 0) {
            var moveStep = (now - moveStartedAt) * tankMoveSpeed;
            lastMoveTime += now - moveStartedAt;
            myUnit.move(moveStep);
        } else {
            lastMoveTime = 0;
        }
        
        
        realMoveStoppedAt = 0;
        moveStartedAt = now;
    } else {
        moveStartedAt = 0;
        realMoveStartedAt = 0;
        if (realMoveStoppedAt == 0) {
            realMoveStoppedAt = now;
        }
    }

    var cmd = 'st';
    if (move) {
        cmd = 'mv';
    }
    var message = {cmd: cmd, dr: myUnit.direction};
    var json = JSON.stringify(message);
    if (WsConnection && WsConnection.readyState == WsConnection.OPEN) {
        WsConnection.send(json);
    }
}

function bulletHitPlayer (tank, bullet) {

    bullet.kill();

}


function fire () {
    var message = {cmd: 'f'};
    var json = JSON.stringify(message);
    if (WsConnection && WsConnection.readyState == WsConnection.OPEN) {
        WsConnection.send(json);
    }
}

function render () {

}

function gofull() { 
    if (game.scale.isFullScreen) {
        game.scale.stopFullScreen();
    } else {
        game.scale.startFullScreen(false);
    }
}

function updateLeaderBoard() {
    var leaderBoard = [];
    for (var clientId in clients) {
        if (clients.hasOwnProperty(clientId)) {
            var unit = clients[clientId];
            var player = {id: clientId, nick: unit.nickname, lvl: unit.lvl};
            leaderBoard.push(player);
        }
    }

    leaderBoard.sort(function(a, b) {
        if (a.lvl < b.lvl) {
            return 1;
        }
           
        if (a.lvl > b.lvl) {
          return -1;
        }
            
        return 0;
    });

    for (var i = 0; i < leaderboardTexts.length; i++) {
        var text = "";
        if (i < leaderBoard.length) {
            var player = leaderBoard[i];
            text = (i+1) + ". " + player.nick + ": " + player.lvl;
        }
        leaderboardTexts[i].setText(text);
    }

    return leaderBoard;
}

var TankUnit = function (game, data) {
    this.game = game;
    this.data = data;
    this.x = data.x;
    this.y = data.y;
    this.rawX = this.x;
    this.rawY = this.y;
    this.direction = data.dr;
    this.lastDirection = data.dr;
    this.lvl = data.lvl;
    this.nickname = "";
    if (data.n) {
        this.nickname = data.n;
    }
    this.nickOffsetY = 32;
    
    this.tank = this.game.add.sprite(this.x, this.y, 'atlas', this.getTankSpriteName());
    this.tank.anchor.setTo(0.5, 0.5);
    this.tank.width = 32;
    this.tank.height = 32;
    this.tank.angle = getAngleFromDirection(this.direction);

    this.shadow = this.game.add.sprite(this.x, this.y, 'atlas', 'shadow.png');
    this.shadow.anchor.setTo(0.5, 0.5);
    this.shadow.scale.setTo(0.5, 0.5);

    this.tank.bringToTop();

    this.lvlText = this.game.add.text(this.x, this.y, this.lvl, {
        font: "12px Arial",
        fill: "#fff",
        align: "center"
    });
    this.lvlText.anchor.setTo(0.5, 1);

    this.nickText = this.game.add.text(this.x, this.y + this.nickOffsetY, this.nickname, {
        font: "12px Arial",
        fill: "#fff",
        align: "center"
    });
    this.nickText.anchor.setTo(0.5, 1);
}

TankUnit.prototype.getTankSpriteName = function() {
    return 'tank_red.png';
};


TankUnit.prototype.setNewData = function(data) {
    this.x = data.x;
    this.y = data.y;
    this.rawX = data.x;
    this.rawY = data.y;
    this.lastDirection = this.direction;
    this.direction = data.dr;
    this.lvl = data.lvl;
    if (data.n) {
        this.nickname = data.n;
    }

    this.update();
};

TankUnit.prototype.update = function() {
    this.tank.x = this.x;
    this.tank.y = this.y;
    this.tank.angle = getAngleFromDirection(this.direction);
    this.shadow.x = this.x;
    this.shadow.y = this.y;
    this.lvlText.x = this.x;
    this.lvlText.y = this.y;
    this.lvlText.setText(this.lvl);

    this.nickText.x = this.x;
    this.nickText.y = this.y + this.nickOffsetY;
    this.nickText.setText(this.nickname);
};

TankUnit.prototype.updateLvl = function(lvl) {
    this.lvl = lvl;
    this.lvlText.setText(this.lvl);
};

TankUnit.prototype.hide = function() {
    this.tank.visible = false;
    this.shadow.visible = false;
    this.lvlText.visible = false;
    this.nickText.visible = false;
};

TankUnit.prototype.destroy = function() {
    this.tank.destroy();
    this.shadow.destroy();
    this.lvlText.destroy();
    if (this.nickText) {
        this.nickText.destroy();
    }
};

TankUnit.prototype.move = function (moveStep) {
    if (this.direction == Phaser.LEFT) {
        this.rawX -= moveStep;
    } else if (this.direction == Phaser.RIGHT) {
        this.rawX += moveStep;
    } else if (this.direction == Phaser.UP) {
        this.rawY -= moveStep;
    } else if (this.direction == Phaser.DOWN) {
        this.rawY += moveStep;
    }

    this.x = Math.round(this.rawX);
    this.y = Math.round(this.rawY);

    this.update();
}




function MyTankUnit(game, data) {
  TankUnit.call(this, game, data);
}

MyTankUnit.prototype = Object.create(TankUnit.prototype);

MyTankUnit.prototype.getTankSpriteName = function() {
    return 'tank_green.png';
};

MyTankUnit.prototype.setNewData = function(data) {
    
    this.lvl = data.lvl;

    var dx = data.x - this.x;
    var dy = data.y - this.y;

    if (Math.abs(dx) < 10 && Math.abs(dy) < 10) {
        this.x += Math.round(dx) / 4;
        this.y += Math.round(dy) / 4;
    } else {
        this.x = data.x;
        this.y = data.y;
    }
    
    this.rawX = this.x;
    this.rawY = this.y;

    this.lastDirection = this.direction;
    this.direction = data.dr;

    if (data.n) {
        this.nickname = data.n;
    }


    this.update();
};
